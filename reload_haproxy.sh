#!/bin/bash

# Create a server_state file and write server state to this file (in case of reload)
socat /tmp/haproxysocket - <<< "show servers state" > server_state
# (Re)Start HAProxy
haproxy -D -f /etc/haproxy/haproxy.cfg -p /var/run/haproxy.pid -sf $(cat /var/run/haproxy.pid)